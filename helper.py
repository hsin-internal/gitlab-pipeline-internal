import json
import os
import csv

env = 'Gitlab'

#read json file from given location and load it to python dictionary
def ReadJSONFile(Filelocation):
    if Filelocation is None:
        return None
    with open(Filelocation) as file:
        # Read the file contents
        json_str = file.read()
        JSON_File = json.loads(json_str)
    return JSON_File

def ReadJSONFileFromCurrentDirectory(Filelocation):
    # Get the current directory path
    current_dir = os.getcwd()

    # Construct the file path by joining the current directory path and the Filelocation
    file_path = os.path.join(current_dir, Filelocation)

    # Check if the file exists
    if not os.path.exists(file_path):
        raise FileNotFoundError(f"JSON file '{Filelocation}' not found in the current directory.")

    # Read the file and load its contents as JSON
    with open(file_path) as file:
        json_str = file.read()
        json_data = json.loads(json_str)

    return json_data

#
def getConfigVarValue(configKey):
    if (env=='local'):
        return configKey
    else:
        return os.environ.get(configKey)


#convert json data to csv and write to given location
def convertJSONToCsvFile(JSONdata,CsvFilePath):

    # Extracting keys from the first JSON object to use as CSV header
    header = JSONdata[0].keys()

    # Open the file in write mode and create a CSV writer
    with open(CsvFilePath, 'w', newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=header)

        # Write the CSV header
        writer.writeheader()

        # Write the data to the CSV file
        writer.writerows(JSONdata)

    print(f"CSV file '{CsvFilePath}' created successfully.")


